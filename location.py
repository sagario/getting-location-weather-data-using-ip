
from urllib2 import urlopen
from contextlib import closing
import json


url= 'http://ip-api.com/json'
response= urlopen(url)
location = json.loads(response.read())

location_city = location['city']
location_zip = location['zip']
print (location_city)
url2= 'http://api.openweathermap.org/data/2.5/weather?lat=%7.4f&lon=%7.4f&APPID=c3dae359f90443db69dd233e25b9875d' %(location['lat'], location['lon'])
sunny= urlopen(url2)
wthrdata= json.loads(sunny.read())

temp= wthrdata['main']
sky= wthrdata['weather'][0]
wnd= wthrdata['wind']


temperature=1.8*(temp['temp']-273)+32
humidity= temp['humidity']
windspeed=wnd['speed']
wid= sky['id']

print (temperature, humidity, windspeed, wid)
print (wthrdata)


#weatherid:
#20X	thunder storm stuff
#30X	drizzle
#50X	rain
#60X    snow
#70X    Atmosphere conditions